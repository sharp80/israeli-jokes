package com.IsraeliJokes;

import android.graphics.Bitmap;

public class CategoryEntry {
	public static final String TEXT_LIST = "list";
	public static final String THUMBS_LIST = "thumbs";
			
	public String Name = null;
	public String Id = null;
	public Bitmap CatBitmap = null;
	public Bitmap CatBitmapThumb = null;
	public String DisplayLayoutType;


}
